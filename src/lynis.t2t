Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4



==Suggestions d'améliorations avec lynis==[lynis]
[Lynis https://github.com/CISOfy/lynis] est un outil qui va analyser votre
système pour y détecter d'éventuelles erreurs de configuration pouvant entacher
la sécurité de votre serveur. De plus, il peut vous proposer des idées
d'amélioration.

Afin de l'utiliser, installez-le puis lancez le premier scan : 


```
# pkg_add lynis
# lynis --checkall
```


Vous pourrez voir de nombreux messages apparaître. Consultez le fichier 
``/var/log/lynis-report.dat`` afin de lire le rapport calmement : 

```
# less /var/log/lynis-report.dat
```


On pourra lire par exemple comme suggestions : 


```
suggestion[]=SSH-7408|Consider hardening SSH configuration|AllowTcpForwarding (YES --> NO)|-|
suggestion[]=SSH-7408|Consider hardening SSH configuration|ClientAliveCountMax (3 --> 2)|-|
suggestion[]=SSH-7408|Consider hardening SSH configuration|Compression (YES --> NO)|-|
```


Pour voir uniquement les suggestions, utilisez : 

```
# grep suggestion /var/log/lynis-report.dat
```
