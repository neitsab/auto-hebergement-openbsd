Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8


Devenir responsable de ses communications est un pas de géant vers l'autonomie
et la liberté.
Cette partie explique l'installation d'un serveur de courriel à la maison.
Aucune base de données ne sera utilisée, puisque cela ne représente aucun
intérêt en auto-hébergement. Restons simples !

La mise en place d'un serveur mail est souvent considérée comme délicate. On va
donc détailler l'installation pas à pas suivant les étapes suivantes : 

- Configuration d'enregistrements DNS particuliers ;
- Création des certificats pour sécuriser l'authentification avec le
  serveur ;
- Configuration d'opensmtpd, qui se charge d'envoyer et recevoir votre courrier ;
- Configuration de dovecot, qui permet la réception du courrier avec un client comme 
[Thunderbird https://www.mozilla.org/fr/thunderbird/] ;
- Faire le nécessaire pour que vos messages ne soient pas considérés comme des
  spams ;
- Installer un antispam.


Nous verrons en passant comment ajouter de nouveaux comptes mail sur votre
serveur et comment configurer votre client de messagerie pour l'utiliser.


==Configuration de votre zone DNS pour les mails==

Le système de mail passe par des enregistrements qui lui est propre. Nous devons
donc procéder à quelques modifications chez notre
[registre #registre] ou dans notre [zone #principes-DNS]. Ajoutez deux nouveaux champs : 

- Un champ de type A qui pointe vers votre IP (sans doute déjà présent): 
```
&NDD A &IPV4
```
Bien sûr, remplacez ``&IPV4`` par votre IP.
- Un champ de type MX qui pointe vers le A précédent 
```
&NDD. MX 1 &NDD.
```

Notez l'importance du "``.``" final.


Si vous disposez d'une IPv6, la configuration est identique, il suffit seulement
d'enregistrer un champ ``AAAA`` à la place du champ ``A``.


Vérifiez auprès de votre [registre #registre] qu'un "//reverse DNS//" est bien configuré
pour votre adresse IP. Cela se configure à part, ça n'a rien à voir avec la
//zone//. Un petit mail à votre registre permettra de savoir comment s'y prendre
si vous ne trouvez pas ;).


Ça sera tout pour cette partie.


==Création des certificats==
Ces certificats permettront de chiffrer la communication entre votre serveur et
le logiciel qui récupère vos
mails. De plus, cela rend chaque communication authentique.

Pour obtenir ou créer des certificats, je vous renvoie à la 
[partie consacrée #sslcert]. Que vous utilisiez ``letsencrypt`` ou bien des
certificats auto-signés n'a pas grande importance, les deux fonctionnent très
bien. Veillez juste à bien prendre note de l'emplacement
des certificats sur votre serveur.

Par la suite, nous feront comme si nous utilisions les certificats obtenus avec
letsencrypt.

==Configuration d'Opensmtpd==
Opensmtpd est le serveur mail par défaut sur OpenBSD. Il est déjà
installé, reste à le configurer. 

Cependant, avant toutes choses, ouvrez et redirigez les ports suivants :
25 (smtp), 587 (submission) et 993 (imaps).


Pour configurer opensmtpd, on édite
``/etc/mail/smtpd.conf`` dans lequel nous mettrons par exemple : 

```
table aliases file:/etc/mail/aliases

pki &NDD &SSLKEY
pki &NDD &SSLCERT
ca &NDD &SSLCERT


# Deliver
listen on lo0
listen on re0 port smtp hostname &NDD tls pki &NDD
listen on re0 port submission hostname &NDD tls-require pki &NDD auth

accept from local for local  alias <aliases> deliver to maildir "~/Maildir"
accept from any for domain "&NDD" deliver to maildir "~/Maildir"

# Relay
accept from local for any relay 
```

Vous n'avez quasiment rien à modifier dans ce fichier, mis à part ``&NDD`` à remplacer par
votre nom de domaine.

	STOOOP! On veut des détails!


Regardons les lignes de ce fichier les unes après les autres. Vous
avez certainement déjà remarqué que la syntaxe ressemble beaucoup à celle du
parefeu.

- ``table aliases ...`` : On précise dans quel fichier se trouvent les alias
  entre les utilisateurs. Ce fichier permet de [faire suivre des messages #admin].
- ``pki ...`` : On indique où se trouve la clé et le certificat correspondant
  servant à identifier le serveur. Modifiez les emplacements selon ce que vous
  avez obtenu [dans le paragraphe sur la gestion des certificats #sslcert].
- ``ca ...`` : On indique où se trouve le certificat de l'autorité
  (letsencrypt). Cette ligne n'est pas présente si vous utilisez un certificat
  auto-signé.


Les lignes suivantes servent à l'envoi du courrier : 

- ``listen on lo0`` : On écoute en local, principalement pour les messages émis par le système. 
- La ligne suivante signifie qu'on écoute le port SMTP sur l'interface
  réseau re0 (trouvée avec la commande ``ifconfig``). Les
  messages sont destinés à votre nom de domaine. Enfin, on active le chiffrement
  TLS à
  la demande avec la clé générée auparavant.

```
listen on re0 port smtp filter filter-pause hostname &NDD tls pki &NDD
``` 
- Cette ligne faire la même chose que la précédente, avec le chiffrement sur le
  port IMAPS pour une sécurité lors de la récupération des messages avec un
  logiciel comme Thunderbird :

```
listen on re0 port submission hostname &NDD tls-require pki &NDD auth
``` 



Les lignes suivantes concernent la réception du courrier : 

- On distribue le courrier local pour chaque alias indiqué dans le
  fichier ``/etc/mail/aliases``, autrement dit aux utilisateurs du système.

```
accept from local for local  alias <aliases> deliver to maildir "~/Maildir"
``` 

- On distribue le courrier venant de l'extérieur.

```
accept from any for domain "&NDD" deliver to maildir "~/Maildir"
```


Et enfin on envoie le courrier destiné aux autres serveurs : 

- ``accept from local for any relay``



Nous passons maintenant à une étape simple mais importante afin que les mails
soient correctement émis. Il faut indiquer dans le fichier
``/etc/myname`` votre nom de domaine sur une seule ligne: 

```
&NDD
```

Nous pouvons maintenant activer et relancer le serveur smtpd : 

```
# rcctl enable smtpd
# rcctl restart smtpd
```

Voilà pour opensmtpd.


==Dovecot pour l'IMAP==[dovecot]
Dovecot va être utilisé comme serveur IMAP, afin de pouvoir récupérer
son courrier à partir d'un client comme Thunderbird.

On installe dovecot comme d'habitude : 
```
# pkg_add dovecot
```

On édite maintenant le fichier ``/etc/dovecot/local.conf`` pour y mettre
le contenu suivant : 

```
# On écoute en IPV4 et IPV6.
listen = *, [::]

# On stocke en Maildir
mail_location = maildir:~/Maildir

# imap > pop
protocols = imap

# Sécurisation. Editez ces lignes
ssl = yes
ssl_cert = </etc/ssl/acme/&NDD-fullchain.pem
ssl_key </etc/ssl/acme/private/&NDD-privkey.pem
disable_plaintext_auth = yes

# méthodes d'authentification
passdb {
        driver = bsdauth
}

userdb {
        driver = passwd
}
```

Pensez à adapter l'emplacement des certificats aux variables ``ssl_cert`` et
``ssl_key``. Chaque section est commentée pour que vous compreniez à quoi elles
servent.

Afin que dovecot fonctionne correctement, il faut maintenant éditer le
fichier ``/etc/login.conf`` pour ajouter quelques lignes : 
(voir le fichier
``/usr/local/share/doc/pkg-readmes/dovecot*``) 

```
dovecot:\
                :openfiles-cur=512:\
                :openfiles-max=2048:\
                :tc=daemon:
```

On prend en compte les changements récents avec la commande suivante : 

```
# [ -f /etc/login.conf.db ] && cap_mkdb /etc/login.conf

```

Pour terminer cette partie, on active dovecot et on relance les
différents éléments constituant le serveur mail.

```
# rcctl enable dovecot
# rcctl start dovecot
# rcctl restart smtpd
```

Il vous est désormais possible d'utiliser un client de messagerie afin de
consulter votre courrier.


==Configurer son client de messagerie==[mailclient]

Pour consulter vos mails sur le serveur, vous pouvez utiliser un client
de messagerie comme l'excellent [Thunderbird https://www.mozilla.org/fr/thunderbird/], 
logiciel-libre respectueux de
votre vie privée.

Voici les paramètres qu'il faudra indiquer au logiciel pour envoyer et
recevoir des courriels. Notez que tous ne vous seront peut-être pas
demandés : 

- Nom du serveur : ``&NDD``
- Adresse électronique : ``toto@&NDD``
- Nom d'utilisateur : l'identifiant choisi à la 
[création d'un compte mail #newmailaddress].
- Serveur entrant : IMAP 
    - port : 993
    - SSL : SSL/TLS
- Serveur sortant : SMTP 
    - port : 587
    - SSL : STARTTLS


Vous souhaiterez peut-être plutôt utiliser un [webmail #webmail], afin d'accéder
à votre messagerie à partir d'un navigateur web. 



==Ne pas être mis dans les spams==
En l'état, vous pouvez recevoir et envoyer des messages. Cependant, il se peut
que certains serveurs de messagerie considèrent vos mails comme des spams.
Heureusement, il existe quelques petites manipulations pour rendre vos messages
légitimes. Nous allons les détailler dans les paragraphes suivants. Gardez à
l'esprit qu'elles se complètent sans se suffire à elles-mêmes.


===Reverse DNS===
Chez votre registrar, ajoutez à l'IP de votre serveur un //reverse DNS// ou en
français //DNS inverse//. 

Alors que votre nom de domaine est relié à l'IP du serveur, il faut aussi
configurer la réciproque, c'est-à-dire relier à votre IP le nom de domaine.

===SPF===

Ajoutez un champ DNS de type SPF dans votre zone DNS (chez votre registrar ou sur votre [serveur de noms serveur-de-nom] si vous l'hébergez aussi) tel que celui-ci : 
```
&NDD.   SPF "v=spf1 a mx ~all"
```

ou bien sous forme de champ TXT si le SPF n'est pas disponible : 
```
&NDD. TXT "v=spf1 a mx ~all"
```

===Signature DKIM===[DKIM]
Cette technique consiste à signer les messages émis par le serveur à l'aide
d'une clé privée. On ajoute ensuite dans un champ DNS la clé publique qui
permettra au destinataire de vérifier que le mail reçu provient bien de votre
serveur avec la signature.


	Moi pas compris!


Je reprends. Nous allons créer une clé //privée// et une clé //publique//.

La clé //privée// servira à signer les messages. On
l'appelle "**privée**" car vous devez être la seule personne capable de signer
(comme pour vos chèques ou vos impôts).
La clé //publique// est là pour vérifier que la signature est bien authentique. On
peut voir ça comme une pièce de puzzle unique, qui est la seule à pouvoir entrer
dans l'empreinte créée par la signature.

Les commandes suivantes permettent de fabriquer la paire de clés qui servira à
signer les mails émis :

+ Création du dossier pour les clefs
```
# mkdir -p /etc/dkimproxy/private                    
```

+ On protege le dossier
```
# chown _dkimproxy:wheel /etc/dkimproxy/private 
```

+ On modifie les permissions
```
# chmod 700 /etc/dkimproxy/private                   
```

+ On va dans le dossier
```
# cd /etc/dkimproxy/private                          
```

+ On génère les clefs
```
# openssl genrsa -out private.key 1024               
# openssl rsa -in private.key -pubout -out public.key
```

+ On protège les clefs
```
# chown _dkimproxy:wheel private.key public.key      
# chmod 600 private.key
```


On peut maintenant installer dkimproxy comme d'habitude : 

```
# pkg_add dkimproxy
```

Afin de configurer la signature des messages envoyés, il faut éditer le ficher
``/etc/dkimproxy_out.conf`` ainsi : 

```
listen    127.0.0.1:10027
relay     127.0.0.1:10028
domain    &NDD
signature dkim(c=relaxed)
signature domainkeys(c=nofws)
keyfile   /etc/dkimproxy/private/private.key
selector  pubkey
```

	Euh, c'est quoi tout ça?

Quelques menues explications : 

- Les lignes ``listen`` et ``relay`` indiquent respectivement les ports locaux
  où dkimproxy recevra les mails à chiffrer et où il les fera suivre. 
- ``domain`` décrit votre nom de domaine : à modifier selon votre cas. 
- ``keyfile`` permet d'indiquer où se trouve le chemin vers la clé servant à
  signer les mails. 
- ``selector`` définit l'identifiant qui sera présent dans le champ DNS que l'on
  va définir ensuite.


Bien, reste à indiquer à opensmtpd de signer les mails. On va donc ajouter dans
le fichier ``/etc/mail/smtpd.conf`` une ligne pour écouter sur le port 10028 en
local, afin d'envoyer les mails que dkimproxy aura signé. On leur colle
l'étiquette "DKIM" pour les repérer ensuite.

```
listen on lo0 port 10028 tag DKIM
```

Les messages qui auront l'étiquette "DKIM" peuvent être envoyés. On n'envoie
plus les mails s'ils ne sont pas passés par dkimproxy. 

```
accept tagged DKIM for any relay
```

Enfin, les messages pas encore signés sont envoyés à dkimproxy : 
```
accept from local for any relay via smtp://127.0.0.1:10027
```

Le fichier ``/etc/mail/smtpd.conf`` ressemble désormais à ça : 

```
table aliases file:/etc/mail/aliases

pki &NDD &SSLKEY
pki &NDD &SSLCERT
ca &NDD &SSLCERT

# Deliver
listen on lo0
listen on lo0 port 10028 tag DKIM
listen on re0 port 25 hostname mail.&NDD tls pki mail.&NDD
listen on re0 port 587 hostname mail.&NDD tls-require pki mail.&NDD auth

accept from local for local  alias <aliases> deliver to maildir "~/Maildir"
accept from any for domain "&NDD" deliver to maildir "~/Maildir"

# Relay
# dkim tagged can be sent
accept tagged DKIM for any relay
# if not dkim tagged, send it to dkimproxy
accept from local for any relay via smtp://127.0.0.1:10027
```

Ça va? Vous suivez toujours? Je vois à votre regard pétillant que vous attendez
la fin avec impatience!

Pour terminer, nous allons ajouter un nouveau champ dans vos DNS auprès de votre
registrar ou dans votre zone. Eh oui, encore! On va en réalité indiquer le nécessaire pour pouvoir
vérifier la signature des messages qui auront un drapeau "pubkey".

Il s'agira d'un champ DKIM ou TXT selon ce qui est disponible.
Remplissez-le ainsi :  
- Nom de domaine : ``pubkey._domainkey.&NDD``.
- Contenu : ``v=DKIM; k=rsa; p=...``
Recopiez à la place des points de suspension le contenu du fichier
``public.key``, que vous pouvez afficher avec la commande
``cat`` :

```
# cat /etc/dkimproxy/private/public.key
```


Finalement, activez dkimproxy puis rechargez opensmtpd avant de
tester si vous avez réussi à configurer correctement l'envoi de vos mails.

```
# rcctl enable dkimproxy_out
# rcctl start dkimproxy_out
# rcctl restart smtpd
```

Pour vérifier que vous n'êtes pas en spam, suivez les indications 
du site [mail-tester https://www.mail-tester.com/]. Vous allez envoyer un
message à l'adresse donnée, et normalement, vous devriez obtenir au moins une
note de 9/10 : 

[img/mail-tester1.png]

[img/mail-tester2.png]

Il se peut qu'on vous parle d'un enregistrement dmarc. Libre à vous de l'ajouter
à vos DNS, mais ce n'est pas obligatoire. À vrai dire, le score obtenu est déjà
meilleur qu'avec nombre de "grands" services de messagerie.



==Ajouter un nouveau compte mail==
Un compte mail est en fait un simple compte d'utilisateur. Vous pouvez
donc en créer un nouveau avec la commande ``adduser`` détaillée dans 
[ce paragraphe #newmailaddress]. Il faudra juste veiller à
donner à l'utlisateur le shell ``/sbin/nologin`` par mesure de sécurité. Ainsi, il ne
pourra pas exécuter de commandes sur le serveur.

===Redirection de mail===[mailalias]
Il est simplissime de transférer les mails reçus sur une adresse vers
un autre compte de messagerie.

Par exemple, vous disposez d'une adresse 
bibi@&NDD et souhaitez que tous les messages reçus par bibi 
soient transférés automatiquement à jean-eudes@openmailbox.org.

Pour ça, il suffit d'éditer le fichier ``/etc/mail/aliases``, puis d'y
ajouter une ligne comme celle-ci : 

```
bibi: jean-eudes@openmailbox.org
```

Vous pouvez aussi faire des redirections internes au serveur, très utile pour l'adminstration système:

```
root: jean
hostmaster: root
postmaster: root
webmaster:  arthur
```

De façon générale, ça donne : 

```
utilisateur: adresse1.mail.com, adresse2.mail.com
```

Afin que ce changement soit pris en compte, il reste 
à lancer les commandes suivantes :  

```
# newaliases
# rcctl restart smtpd
```

C'est tout! Je vous l'avais dit que c'était simple.




==Installer un antispam==

Votre serveur n'est actuellement pas à l'abri des spams. Nous allons donc voir
une méthode simple avec [spamassassin #spamassassin] pour s'en protéger.

%FIXME
%Vous
%pouvez aussi utiliser l'alternative tout aussi efficace [rspamd #rspamd].

Notez qu'il existe des
techniques plus efficaces mais nettement plus difficiles à mettre en place
puisque les gros fournisseurs de messagerie disposent de nombreuses adresses IP.
Pour ceux que le sujet intéresse, vous pouvez consulter la documentation de
spamd et de [bgpd http://bgp-spamd.net/client/]. Nous en parlerons rapidement
dans un [second temps #spamd].

D'autres programmes très complets pour lutter contre le spam existent comme 
[rspamd https://www.rspamd.com/] si vous souhaitez vous renseigner.

	Houla, ça fait beaucoup pour faire la même chose !

Pour ma part, je trouve rassurant de lire que l'équipe OpenBSD utilise
[spamassasin #spamassassin] et [spamd #spamd] pour protéger ses 
[listes de diffusion https://www.openbsd.org/mail.html].



===Spamassassin===[spamassassin]
%!include: spamassassin.t2t

%== Antispam avec rspamd ==[rspamd]
%%!include: rspamd.t2t

===Antispams et dovecot===[sieve]
%!include: sieve.t2t

=== Antispam avec spamd ===[spamd]
%!include: spamd.t2t


==Vérifier que tout fonctionne bien==

Vous voudrez peut-être tester votre serveur mail après tous ces efforts. Vous
l'avez bien mérité. Vous pouvez bien entendu écrire à des amis, mais cela peut
poser des soucis : 

- Il faudra attendre leur réponse ; 
- Ils ne vous retourneront pas toutes les informations dont vous pourriez avoir
  besoin ;
- Il faut avoir des amis.


Il existe heureusement des robots auxquels ont peut écrire un mail, qui vous
répondront très vite en vous donnant de précieuses informations. Nous avons déjà
parlé de [mail-tester http://www.mail-tester.com] un peu plus haut. Il existe aussi les adresses
suivantes : 

- ``echo@generic-nic.net`` 
- ``echo@nic.fr``
- ``autoreply@dmarctest.org``


Vous en trouverez d'autres sur 
[cette page http://www.bortzmeyer.org/repondeurs-courrier-test.html].

